Pod::Spec.new do |spec|
  spec.name         = 'HKManager'
  spec.version      = '1.1.3'
  spec.license      = { :type => 'BSD' }
  spec.homepage     = 'https://bitbucket.org/vcook-mobiquity/vcs-mobile-ios-healthkit'
  spec.authors      = { 'Mobiquity' => 'wwallace@mobiquityinc.com' }
  spec.summary      = 'HealthKit upload framework'
  spec.source       = { :git => 'https://bitbucket.org/vcook-mobiquity/vcs-mobile-ios-healthkit.git', :tag => 'v1.1.3' }
  spec.source_files = 'HKManager'
  spec.framework    = 'HealthKit'
  spec.dependency 'AFNetworking', '2.6.3'
  spec.dependency 'Mantle', '> 2'
end
